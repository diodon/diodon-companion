# Brief description

This repository is a companion to *pydiodon* and *cppdiodon* which are librairies (respectively in python and C++) which provide some functions for implementing methods of linear dimension reduction, presented in the companion document https://arxiv.org/abs/2209.13597.

# What it contains #

This git contains two types of files:

- datasets for testing diodon (py and cpp) 
- jupyter notebooks as tutorials for some methods (PCA, CoA, MDS)

It contains no code, and is a companion to projects to   
- *pydiodon*, see https://gitlab.inria.fr/diodon/pydiodon
- *cppdiodon*, see https://gitlab.inria.fr/diodon/cppdiodon


# Installation

The installation is available for *Linux Ubuntu* system 20 or above. It can be adapted for other distributions.  


It is advised, but not compulsory, to install `diodon-companion` in a directory named *diodon* created therefore.

```sh
# go into any directory on your computer (here named diodon)
$ cd diodon

# clone the git diodon-companion
$ git clone git@gitlab.inria.fr:diodon/diodon-companion.git
```

# How it works

```sh
# go into directory diodon-companion
$ cd [...]/diodon/diodon-companion

# for running notebook <a_notebook>.ipynb
$ jupyter notebook <a_notebook>.ipynb &
```

# For PCA
.
- The dataset is *ade4_doubs_env.tsv*,
- several jupyter notebooks are available:
    - *pca_with_diodon.ipynb* 
    - *pca_diodon_iris.ipynb* 
    - *pca_how_it_works.ipynb* 
    - *pca_random_matrices_dioson.ipynb*.

## file ade4_doubs_env.tsv

This is the file *doubs$env* provided by ade4. It consists in 30 sites along the Doubs river, with 11 environmental variables. See    
https://rdrr.io/cran/ade4/man/doubs.html   
for more details.   

It is available here as an ascii file with tab as separators, with rownames and columnnames.   

It can be loaded with command 
``` python
>>> import pydiodon as dio
>>> dataset = "ade4_doubs_env"
>>> A, rownames, colnames = dio.load_dataset(dataset) 
```
- The features are loaded as a 2D numpy array $A$
- the rownames are loaded as a list *rownames* 
- the column names are loaded as a list *colnames* 

# For CoA


# For MDS

# ID card

`diodon-companion` is part of ADT program `Diodon` at INRIA BSO (SED, EP Hiepacs/Concace, EPC Pleiade, Inrae BioGeCo).


#### Contributors

Olivier Coulaud     
Alain Franc   
Jean-Marc Frigerio   
Rémi Peressoni    
Florent Pruvost

#### Maintainer

Alain Franc, alain.franc@inrae.fr

**Started:** 22.12.12   
**Version:** 23.03.13

