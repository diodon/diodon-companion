#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import os
import sys
import time
import numpy as np
import matplotlib.pyplot as plt
import pydiodon as pydio
import cppdiodon as cppdio

# pas nécessaire de le redéfinir : dans pydiodon
"""
def plot_cloud(X):
    nbin = 256
    Ap1 = X[:,0]
    Ap2 = X[:,1]
    H, _, _ = np.histogram2d(Ap1, Ap2, bins=nbin)
    H = np.log(1+H)
    plt.imshow(H)
"""

# oui,  utile car il y a un plot_eigen mais seulement pour une SVD ... 
def plot_eigen(S1, S2):
    S1 = np.round(S1[:S1.shape[0]])
    S2 = np.round(S2[:S2.shape[0]])
    plt.yscale('log')
    plt.plot(S1,c="blue")
    plt.plot(S2,c="green")
    plt.show()

def mds_compare(disfile, rank):
	
	# repris ça car les fonctions de chargement existent dans pydiodon ...
	# c'est "presque" pareil ... en une seule fonction
    #A = pydio.loadfile(filename=file, datasetname=dataset)
    #A = np.asanyarray(A, dtype='float32', order='F')

    print("file is", disfile)
    print("loading file")
    A, rn, cn = pydio.loadfile(disfile, fmt="ascii", rownames=True, colnames=True)
    print("file loaded!")
    
    print("\nrunning MDS with pydiodon")    
    start_time = time.time()
    Xp, Sp = pydio.mds(A, k=rank, meth='grp')
    print("--- pydiodon mds with pydiodon took %s seconds ---" % (time.time() - start_time))
    pydio.plot_components_scatter(Xp)

    print("\nrunning mds with cppdiodon bound to python")
    start_time = time.time()
    Xc, Sc = cppdio.mds(A, k=rank, meth='grp', nt=8, ng=2)
    print("--- cppdiodon mds %s seconds ---" % (time.time() - start_time))
    pydio.plot_components_scatter(Xc)


    plt.plot(np.log(Sp),c="blue")
    plt.plot(np.log(Sc),c="green")
    plt.title("blue: python; green: C++")
    plt.show()
    
    
# ----------------------------------------------------------------------
#
#			main()
#
# ----------------------------------------------------------------------

disfile = 'guiana_trees.sw.dis'		# c'est son nom dans diodon_companion, en ascii (petit jeu de données)
rank = 500 # mis plus bas à cause des vp négatives ...
print("disfile is", disfile)
mds_compare(disfile, rank=rank)


#mds_compare('/beegfs/diodon/gordon/10V-RbcL_S74.h5', 'distance', 3200)
#mds_compare('/beegfs/diodon/gordon/26_rbcL.h5', 'distance', 3200)
#mds_compare('/beegfs/diodon/gordon/L1-L10/L1.h5', 'distance', 3200)
#mds_compare('/beegfs/diodon/gordon/L1-L10/L6.h5', 'distance', 3200)

